package pl.dmcs.dbartos.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import pl.dmcs.dbartos.configuration.SecurityConfiguration;
import pl.dmcs.dbartos.configuration.TestConfiguration;
import pl.dmcs.dbartos.service.UserService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {TestConfiguration.class, SecurityConfiguration.class})
public class ProfileControllerTest {

    @Autowired
    UserService userService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.webApplicationContext);
        this.mockMvc = builder.build();
    }

    @Test
    public void mainPageShouldReturnHomeViewWithMovies() throws Exception {
        ResultActions resultActions = mockMvc.perform(get("/")).andExpect(status().isOk());
        ModelAndView modelAndView = resultActions.andReturn().getModelAndView();
        Assert.assertEquals("home", modelAndView.getViewName());
        Assert.assertNotNull(modelAndView.getModel().get("movies"));
    }
}
