package pl.dmcs.dbartos.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.dmcs.dbartos.configuration.TestConfiguration;
import pl.dmcs.dbartos.domain.Movie;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.domain.UserRole;
import pl.dmcs.dbartos.utils.BuilderUtils;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = TestConfiguration.class)
public class UserServiceTest {

    private final String junitMovieName = "junitMovieName";
    private final String junitTestLogin = "junitTestLogin";

    @Autowired
    UserService userService;
    @Autowired
    UserRoleService userRoleService;
    @Autowired
    MovieService movieService;

    private User user = BuilderUtils.buildUser(junitTestLogin);
    private Movie movie = BuilderUtils.buildMovie(junitMovieName);


    @Test
    public void registerUserShouldHaveOnlyUserRoles() {
        UserRole adminRole = userRoleService.getUserRole(UserRole.ADMIN);
        UserRole userRole = userRoleService.getUserRole(UserRole.USER);

        User registeredUser = userService.registerUser(user, UserRole.USER);

        Assert.assertTrue(registeredUser.getUserRole().contains(userRole));
        Assert.assertFalse(registeredUser.getUserRole().contains(adminRole));
    }

    @Test
    public void editUserShouldUpdateUserFields() {
        userService.registerUser(user, UserRole.USER);
        String newEmail = "newTestEmail@test.test";
        user.setEmail(newEmail);
        User editedUser = userService.updateUser(user);
        Assert.assertEquals(newEmail, editedUser.getEmail());
    }

    @Test
    public void reserveMovieForUserShouldAddMovieReservationAsNotConfirmed() {
        userService.registerUser(user, UserRole.USER);
        Movie newMovie = movieService.updateOrCreateMovie(movie);
        userService.reserveMovieForUser(newMovie, junitTestLogin);
        User byLogin = userService.findByLogin(junitTestLogin);
        Assert.assertEquals(1, byLogin.getReservedMovies().size());
        Assert.assertFalse(byLogin.getReservedMovies().get(0).getConfirmed());
    }

    @Test
    public void userAfterRegistrationShouldBeDisabled() {
        User registerUser = userService.registerUser(user, UserRole.USER);
        Assert.assertFalse(registerUser.isEnabled());
    }

    @After
    public void post() {
        User user = userService.findByLogin(junitTestLogin);
        if (user != null)
            userService.removeUser(user.getId());

        Movie movieByName = movieService.findMovieByName(junitMovieName);
        if (movieByName != null)
            movieService.removeMovieById(movieByName.getId());
    }

}
