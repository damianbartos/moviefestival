package pl.dmcs.dbartos.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@Import(MovieFestivalConfiguration.class)
public class TestConfiguration {

    @Bean(name = "dataSource")
    @Primary
    public DataSource getDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:DATA_JUNIT_TEST;DB_CLOSE_DELAY=-1;INIT=CREATE SCHEMA IF NOT EXISTS DATA_JUNIT_TEST");
        dataSource.setUsername("sa");
        dataSource.setPassword("");
        return dataSource;
    }

}
