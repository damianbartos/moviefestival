package pl.dmcs.dbartos.utils;

import pl.dmcs.dbartos.domain.Movie;
import pl.dmcs.dbartos.domain.User;

import java.math.BigDecimal;
import java.util.Date;

public class BuilderUtils {
    public static User buildUser(String login) {
        User user = new User();
        user.setLogin(login);
        user.setPassword("123456");
        user.setFirstName("FirstName");
        user.setTelephone("123123123");
        user.setEmail("test@test.test");
        return user;
    }

    public static Movie buildMovie(String name) {
        Movie movie = new Movie();
        movie.setDate(new Date());
        movie.setDescription("test movie for jUnit");
        movie.setName(name);
        movie.setPrice(new BigDecimal(12.22));
        return movie;
    }
}
