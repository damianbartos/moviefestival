package pl.dmcs.dbartos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.domain.VerificationToken;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Integer> {
    VerificationToken findByToken(String token);
    VerificationToken findByUser(User user);
}
