package pl.dmcs.dbartos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dmcs.dbartos.domain.MovieReservation;

@Repository
public interface MovieReservationRepository extends JpaRepository<MovieReservation, Integer> {
}
