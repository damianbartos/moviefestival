package pl.dmcs.dbartos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dmcs.dbartos.domain.PeselNumber;

@Repository
public interface PeselNumberRepository extends JpaRepository<PeselNumber, Integer> {
}
