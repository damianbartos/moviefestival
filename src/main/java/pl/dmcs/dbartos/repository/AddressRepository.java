package pl.dmcs.dbartos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dmcs.dbartos.domain.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {
    Address findByStreetAndNumberAndCityAndPostalCode(String street, String number, String city, String postalCode);
}
