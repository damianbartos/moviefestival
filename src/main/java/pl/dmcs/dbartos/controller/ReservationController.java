package pl.dmcs.dbartos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.dmcs.dbartos.domain.Movie;
import pl.dmcs.dbartos.domain.MovieReservation;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.domain.UserPrinciples;
import pl.dmcs.dbartos.service.MovieReservationService;
import pl.dmcs.dbartos.service.MovieService;
import pl.dmcs.dbartos.service.UserService;
import pl.dmcs.dbartos.utils.pdf.PdfGenerator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

@Slf4j
@Controller
public class ReservationController {

    private final MovieService movieService;
    private final UserService userService;
    private final MovieReservationService movieReservationService;

    @Autowired
    public ReservationController(MovieService movieService, UserService userService, MovieReservationService movieReservationService) {
        this.movieService = movieService;
        this.userService = userService;
        this.movieReservationService = movieReservationService;
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(value = "/reserve/check", method = RequestMethod.POST)
    public String helloWorld(Model model, @RequestParam(value = "movieName") String movieName) {
        log.info("check reservation by user");
        Movie movie = movieService.findMovieByName(movieName);
        model.addAttribute("movie", movieName);
        model.addAttribute("movieObj", movie);
        return "reserve";
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(value = "/reserve/confirm", method = RequestMethod.POST)
    public String confirmReservation(Model model, @RequestParam(value = "movieName") String movieName) {
        log.info("confirm reservation by user");
        UserPrinciples userPrinciples = (UserPrinciples) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findByLogin(userPrinciples.getUsername());
        String username = user.getLogin();
        Movie movie = movieService.findMovieByName(movieName);
        log.info("add {} to user with login - {} - reservation list", movieName, username);
        userService.reserveMovieForUser(movie, username);
        return "redirect:/";
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'USER')")
    @RequestMapping(
            value = "/ticket/{movieReservationId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity generateTicket(Model model,
                                         @PathVariable(value = "movieReservationId") Integer movieReservationId,
                                         HttpServletResponse httpServletResponse,
                                         HttpServletRequest request) throws IOException {
        log.info("generate pdf with ticket informations");
        UserPrinciples userPrinciples = (UserPrinciples) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findByLogin(userPrinciples.getUsername());
        MovieReservation movieReservation = movieReservationService.getMovieReservationById(movieReservationId);
        if (movieReservation == null) {
            log.info("there is no movie reservation with id {}", movieReservationId);
            httpServletResponse.sendRedirect("/error");
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Location", "/error");
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .headers(httpHeaders)
                    .build();
        } else {
            if (user.getReservedMovies().contains(movieReservation) || request.isUserInRole("ADMIN")) {
                log.info("movie reservation found, access grounded for user or admin");
                ByteArrayInputStream pdf = PdfGenerator.generateTicketPdf(user, movieReservation);
                return ResponseEntity
                        .ok()
                        .contentType(MediaType.APPLICATION_PDF)
                        .body(new InputStreamResource(pdf));
            } else {
                log.info("no access to reservation with id {}", movieReservationId);
                httpServletResponse.sendRedirect("/accessDenied");
                HttpHeaders httpHeaders = new HttpHeaders();
                httpHeaders.add("Location", "/accessDenied");
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .headers(httpHeaders)
                        .build();
            }
        }
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/reservations", method = RequestMethod.GET)
    public String allReservations(Model model){
        log.info("get all reservations");
        List<MovieReservation> allReservations = movieReservationService.getAllReservations();
        model.addAttribute("reservations", allReservations);
        return "reservations";
    }

    @PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/reservations/{reservationId}/confirm", method = RequestMethod.GET)
    public String confirmMovieReservation(Model model, @PathVariable(value = "reservationId") int reservationId){
        log.info("confirm movie reservation by administrator");
        movieReservationService.confirmReservation(reservationId);
        return "redirect:/reservations";
    }

}
