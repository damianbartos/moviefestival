package pl.dmcs.dbartos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.service.MovieService;
import pl.dmcs.dbartos.service.UserService;

@Slf4j
@Controller
public class ProfileController {

    final MovieService movieService;
    final UserService userService;

    @Autowired
    public ProfileController(MovieService movieService, UserService userService) {
        this.movieService = movieService;
        this.userService = userService;
    }

    @PreAuthorize(value = "isAuthenticated() and " +
            "(hasAuthority('ROLE_ADMIN') or #userId == authentication.principal.userId)")
    @RequestMapping(value = "/profile/{userId}", method = RequestMethod.GET)
    public String profile(Model model, @PathVariable Integer userId) {
        log.info("get user by userId - {}", userId);
        User user = userService.findByUserId(userId);
        model.addAttribute("user", user);
        return "profile";
    }

    @PreAuthorize(value = "isAuthenticated() and " +
            "(hasAuthority('ROLE_ADMIN') or #userId == authentication.principal.userId)")
    @RequestMapping(value = "/profile/{userId}/edit", method = RequestMethod.POST)
    public String profileUpdate(Model model, User user, @PathVariable Integer userId) {
        log.info("update profile data with - {}", user);
        user = userService.updateUser(user);
        return "redirect:/profile/" + user.getId();
    }

}
