package pl.dmcs.dbartos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.dmcs.dbartos.domain.Movie;
import pl.dmcs.dbartos.service.MovieService;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN')")
public class MovieController {

    private final MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @RequestMapping(value = "/movies")
    public String movies(Model model) {
        List<Movie> allMovies = movieService.getAllMovies();
        model.addAttribute("movies", allMovies);
        return "movies";
    }

    @RequestMapping(value = "/movies/add", method = RequestMethod.GET)
    public String addMovie(Model model) {
        model.addAttribute("movie", new Movie());
        return "movie";
    }

    @RequestMapping(value = "/movies/{movieId}/edit", method = RequestMethod.GET)
    public String editMovie(Model model, @PathVariable Integer movieId) {
        Movie movie = movieService.findMovieById(movieId);
        if (movie == null) {
            return "error";
        }
        model.addAttribute("movie", movie);
        return "movie";
    }

    @RequestMapping(value = "/movies/edit", method = RequestMethod.POST)
    public String editMovieForm(Model model, @ModelAttribute("movie") Movie inputMovie, @RequestParam(value = "image", required = false) MultipartFile file) throws IOException {
        if (file != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("data:image/png;base64,");
            String encodedImage = Base64.getEncoder().encodeToString(file.getBytes());
            if (!encodedImage.isEmpty()) {
                sb.append(encodedImage);
                String imageBase64 = sb.toString();
                inputMovie.setThumbnail(imageBase64);
            }
        }
        Movie movie = movieService.updateOrCreateMovie(inputMovie);
        log.info("marker - {}", movie);
        model.addAttribute("movie", movie);
        return "redirect:/movies";
    }

    @RequestMapping(value = "/movies/{movieId}/remove", method = RequestMethod.GET)
    public String remove(Model model, @PathVariable Integer movieId) {
        movieService.removeMovieById(movieId);
        return "redirect:/movies";
    }


}
