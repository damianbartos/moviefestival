package pl.dmcs.dbartos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.dmcs.dbartos.service.UserService;

@Slf4j
@Controller
@PreAuthorize(value = "hasAuthority('ROLE_ADMIN')")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getAllUsers(Model model){
        log.info("get all users");
        model.addAttribute("users", userService.getAllUsers());
        return "users";
    }

    @RequestMapping(value = "/users/{userId}/edit", method = RequestMethod.GET)
    public String editUser(Model model, @PathVariable(value = "userId") int userId){
        log.info("edit user with id - {}", userId);
        return "redirect:/profile/"+userId;
    }

    @RequestMapping(value = "/users/{userId}/remove", method = RequestMethod.GET)
    public String removeUser(Model model, @PathVariable(value = "userId") int userId){
        log.info("edit user with id - {}", userId);
        userService.removeUser(userId);
        return "redirect:/users";
    }
}
