package pl.dmcs.dbartos.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.domain.UserRole;
import pl.dmcs.dbartos.domain.VerificationToken;
import pl.dmcs.dbartos.service.RecaptchaService;
import pl.dmcs.dbartos.service.UserService;
import pl.dmcs.dbartos.service.VerificationTokenService;
import pl.dmcs.dbartos.utils.registration.OnRegistrationCompleteEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Calendar;

@Slf4j
@Controller
public class SecurityController {

    private final UserService userService;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final VerificationTokenService verificationTokenService;
    private final RecaptchaService recaptchaService;


    @Autowired
    public SecurityController(UserService userService, ApplicationEventPublisher applicationEventPublisher, VerificationTokenService verificationTokenService, RecaptchaService recaptchaService) {
        this.userService = userService;
        this.applicationEventPublisher = applicationEventPublisher;
        this.verificationTokenService = verificationTokenService;
        this.recaptchaService = recaptchaService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model) {
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        log.info("logout user");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registerForm(Model model) {
        log.info("show register form");
        model.addAttribute("user", new User());
        return "register";
    }


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUserAccount(Model model, @ModelAttribute("user") @Valid User userFromForm,
                                      BindingResult bindingResult,
                                      @RequestParam(name = "g-recaptcha-response") String recaptchaResponse,
                                      WebRequest webRequest, HttpServletRequest request,
                                      Errors errors) {
        log.info("validate user result - {}", bindingResult);
        if (bindingResult.hasErrors()) {
            log.info("Returning errors");
            model.addAttribute("user", userFromForm);
            model.addAttribute("errors", bindingResult.getModel());
            return "register";
        }

        log.info("check captcha - {}", recaptchaResponse);
        boolean isCaptchaOk = recaptchaService.verifyRecaptcha(recaptchaResponse);

        if (!isCaptchaOk) {
            log.info("bad chaptcha");
            model.addAttribute("captchaError", "bad captcha");
            model.addAttribute("user", userFromForm);
            return "register";
        }

        log.info("validate and persist user input data - {}", userFromForm);
        User registeredUser = userService.registerUser(userFromForm, UserRole.USER);
        log.info("user - {} - registered", registeredUser);
        try {
            String appUrl = webRequest.getContextPath();
            applicationEventPublisher.publishEvent(new OnRegistrationCompleteEvent
                    (registeredUser, webRequest.getLocale(), appUrl));
        } catch (Exception me) {
            model.addAttribute("errorMessage", "cannot sent confirmation token");
            return "error";
        }
        log.info("confirmation mail send to - {}", registeredUser.getEmail());
        return "redirect:/";
    }

    @RequestMapping(value = "/registrationConfirm", method = RequestMethod.GET)
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {
        VerificationToken verificationToken = verificationTokenService.getVerificationToken(token);
        if (verificationToken == null) {
            model.addAttribute("errorMessage", "invalid token");
            return "error";
        }

        User user = verificationToken.getUser();
        Calendar calendar = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0) {
            model.addAttribute("errorMessage", "token expired");
            return "error";
        }

        userService.enableUser(user.getId());

        return "redirect:/";
    }

}
