package pl.dmcs.dbartos.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.dmcs.dbartos.domain.*;
import pl.dmcs.dbartos.repository.AddressRepository;
import pl.dmcs.dbartos.repository.MovieReservationRepository;
import pl.dmcs.dbartos.repository.PeselNumberRepository;
import pl.dmcs.dbartos.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final MovieReservationRepository movieReservationRepository;
    private final AddressRepository addressRepository;
    private final PeselNumberRepository peselNumberRepository;
    private final UserRoleService userRoleService;
    private final VerificationTokenService verificationTokenService;

    @Autowired
    public UserService(UserRepository userRepository, MovieReservationRepository movieReservationRepository, UserRoleService userRoleService,
                       VerificationTokenService verificationTokenService, AddressRepository addressRepository, PeselNumberRepository peselNumberRepository) {
        this.userRepository = userRepository;
        this.movieReservationRepository = movieReservationRepository;
        this.userRoleService = userRoleService;
        this.verificationTokenService = verificationTokenService;
        this.addressRepository = addressRepository;
        this.peselNumberRepository = peselNumberRepository;
    }

    public User findByLogin(String username) {
        log.info("find user by username - {}", username);
        return userRepository.findByLogin(username);
    }

    public void reserveMovieForUser(Movie movie, String username) {
        log.info("reserve movie - {} - for user - {}", movie, username);
        User user = findByLogin(username);
        MovieReservation movieReservation = new MovieReservation();
        movieReservation.setMovie(movie);
        movieReservation.setConfirmed(false);
        movieReservation.setUser(user);
        movieReservation = movieReservationRepository.save(movieReservation);
        user.getReservedMovies().add(movieReservation);
        userRepository.save(user);
    }

    public User findByUserId(Integer userId) {
        log.info("find user by userId - {}", userId);
        return userRepository.findById(userId);
    }

    public User registerUser(User user, String userRoleName) {
        log.info("register user - {} - with userRole - {}", user, userRoleName);
        UserRole userRole = userRoleService.getUserRole("USER");
        UserRole admin = userRoleService.getUserRole("ADMIN");

        user.getUserRole().add(userRole);
        if (userRoleName.equals("ADMIN")) {
            user.getUserRole().add(admin);
        }
        log.info("hash user - {} - password", user);
        user.setPassword(hashPassword(user.getPassword()));
        return userRepository.save(user);
    }

    public User updateUser(User user) {
        log.info("updated user {}", user);
        User currentUser = userRepository.findByLogin(user.getLogin());
        log.info("current user {}", currentUser);
        currentUser.setEmail(user.getEmail());
        currentUser.setFirstName(user.getFirstName());
        currentUser.setLastName(user.getLastName());
        currentUser.setTelephone(user.getTelephone());

        if (user.getAddress() != null) {
            Address newAddress = addressRepository.findByStreetAndNumberAndCityAndPostalCode(user.getAddress().getStreet(),
                    user.getAddress().getNumber(), user.getAddress().getCity(), user.getAddress().getPostalCode());
            if (newAddress != null) {
                currentUser.setAddress(newAddress);
            } else {
                if (currentUser.getAddress() == null) {
                    // no address currently assigned
                    newAddress = new Address();
                } else {
                    newAddress = addressRepository.findOne(currentUser.getAddress().getId());
                }
                newAddress.setStreet(user.getAddress().getStreet());
                newAddress.setNumber(user.getAddress().getNumber());
                newAddress.setCity(user.getAddress().getCity());
                newAddress.setPostalCode(user.getAddress().getPostalCode());

                newAddress = addressRepository.save(newAddress);
                currentUser.setAddress(newAddress);
            }
        }

        PeselNumber peselNumber = currentUser.getPesel();

        if(user.getPesel()!=null){
            if (peselNumber == null) {
                peselNumber = peselNumberRepository.save(new PeselNumber());
            }
            peselNumber.setPeselNumber(user.getPesel().getPeselNumber());
            peselNumberRepository.save(peselNumber);

            currentUser.setPesel(peselNumber);
        }
        return userRepository.save(currentUser);
    }

    private String hashPassword(String password) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void removeUser(int userId) {
        User user = userRepository.findById(userId);
        verificationTokenService.removeTokenByUserId(user);
        userRepository.delete(userId);
    }

    public void enableUser(int userId) {
        User user = userRepository.findById(userId);
        user.setEnabled(true);
        userRepository.save(user);
    }

    public void promoteToAdmin(int userId) {
        UserRole adminRole = userRoleService.getUserRole("ADMIN");
        User user = userRepository.findById(userId);
        user.getUserRole().add(adminRole);
        userRepository.save(user);
    }
}
