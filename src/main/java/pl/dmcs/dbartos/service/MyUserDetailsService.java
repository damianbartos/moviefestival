package pl.dmcs.dbartos.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.domain.UserPrinciples;
import pl.dmcs.dbartos.domain.UserRole;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@Transactional
public class MyUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public MyUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserPrinciples loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("load user by username - {}", username);
        User user = userService.findByLogin(username);
        user.getUserRole().size();
        List<GrantedAuthority> authorities = buildUserAuthority(user.getUserRole());
        return buildUserForAuthentication(user, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {
        log.info("build user authority based on his roles");
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (UserRole userRole : userRoles) {
            authorities.add(new SimpleGrantedAuthority(userRole.getRole()));
        }
        return new ArrayList<>(authorities);
    }

    private UserPrinciples buildUserForAuthentication(User user, List<GrantedAuthority> authorities) {
        log.info("convert domain.User to spring security .userdetails.User");
        UserPrinciples userPrinciples = new UserPrinciples(user.getLogin(), user.getPassword(), user.isEnabled(),
                true, true, true, authorities);
        userPrinciples.setUserId(user.getId());
        return userPrinciples;

    }
}
