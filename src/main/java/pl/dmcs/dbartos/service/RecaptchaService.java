package pl.dmcs.dbartos.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;

@Slf4j
@Service
public class RecaptchaService {

    private final static String USER_AGENT = "Mozilla/5.0";
    private final String GOOGLE_RECAPTCHA_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";
    private final String SECRET = "6Lfh4ooUAAAAAI1ioGD9jsT_q_Vcy_IqHQ4zAXcQ";

    public boolean verifyRecaptcha(String recaptchaResponse){

        try {
            URL url = new URL(GOOGLE_RECAPTCHA_VERIFY_URL);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

            // add reuqest header
            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", USER_AGENT);
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String postParams = "secret=" + SECRET + "&response="
                    + recaptchaResponse;

            // Send post request
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();

            int responseCode = connection.getResponseCode();
            log.info("Sending 'POST' request to URL : {}", GOOGLE_RECAPTCHA_VERIFY_URL);
            log.info("Post parameters - {}", postParams);
            log.info("Response Code - {}", responseCode);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            log.info("ReCaptcha response - {}", response.toString());

            //parse JSON response and return 'success' value
            JsonReader jsonReader = Json.createReader(new StringReader(response.toString()));
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();

            return jsonObject.getBoolean("success");
        } catch (Exception e) {
            log.error("ReCaptcha response error - {}", e.getMessage());
            return false;
        }
    }
}
