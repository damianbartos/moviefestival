package pl.dmcs.dbartos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.domain.VerificationToken;
import pl.dmcs.dbartos.repository.VerificationTokenRepository;

import javax.transaction.Transactional;

@Service
@Transactional
public class VerificationTokenService {
    final VerificationTokenRepository verificationTokenRepository;

    @Autowired
    public VerificationTokenService(VerificationTokenRepository verificationTokenRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
    }

    public void removeTokenByUserId(User user){
        VerificationToken token = verificationTokenRepository.findByUser(user);
        if(token!=null){
            verificationTokenRepository.delete(token);
        }
    }

    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        verificationTokenRepository.save(myToken);
    }

    public VerificationToken getVerificationToken(String verificationToken){
        return verificationTokenRepository.findByToken(verificationToken);
    }

}
