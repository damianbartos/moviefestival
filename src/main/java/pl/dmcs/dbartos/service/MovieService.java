package pl.dmcs.dbartos.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.dmcs.dbartos.domain.Movie;
import pl.dmcs.dbartos.repository.MovieRepository;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@Transactional
public class MovieService {

    private final MovieRepository movieRepository;

    @Autowired
    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public List<Movie> getAllMovies() {
        log.info("get list of all movies");
        return movieRepository.findAll(new Sort(Sort.Direction.ASC, "name"));
    }

    public Movie findMovieByName(String movieName){
        log.info("find movie by name");
        return movieRepository.findByNameOrderById(movieName);
    }

    public Movie findMovieById(Integer movieId){
        return movieRepository.findOne(movieId);
    }

    public void removeMovieById(Integer movieId){
        movieRepository.delete(movieId);
    }

    public Movie updateOrCreateMovie(Movie inputMovie){
        log.info("update or create movie input - {}", inputMovie);
        Movie movie;
        if(inputMovie.getId()==null){
            // new movie
            movie = movieRepository.save(inputMovie);
            log.info("new movie created - {}", movie);
        }else {
            // update movie
            movie = movieRepository.findOne(inputMovie.getId());
            movie.setName(inputMovie.getName());
            movie.setDescription(inputMovie.getDescription());
            movie.setDate(inputMovie.getDate());
            movie.setPrice(inputMovie.getPrice());
            if(inputMovie.getThumbnail()!=null){
                movie.setThumbnail(inputMovie.getThumbnail());
            }
            log.info("movie updated - {}", movie);
        }
        movie = movieRepository.save(movie);
        return movie;
    }

}
