package pl.dmcs.dbartos.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.dbartos.domain.UserRole;
import pl.dmcs.dbartos.repository.UserRoleRepository;

import javax.transaction.Transactional;

@Slf4j
@Service
@Transactional
public class UserRoleService {
    private final UserRoleRepository userRepository;

    @Autowired
    public UserRoleService(UserRoleRepository userRepository1) {
        this.userRepository = userRepository1;
    }

    public UserRole addUserRole(String roleName){
        UserRole userRole = new UserRole();
        userRole.setRole(roleName);
        return userRepository.save(userRole);
    }

    public UserRole getUserRole(String roleName){
        return userRepository.findByRole("ROLE_"+roleName);
    }
}
