package pl.dmcs.dbartos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dmcs.dbartos.domain.MovieReservation;
import pl.dmcs.dbartos.repository.MovieReservationRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class MovieReservationService {

    @Autowired
    private final MovieReservationRepository movieReservationRepository;

    public MovieReservationService(MovieReservationRepository movieReservationRepository) {
        this.movieReservationRepository = movieReservationRepository;
    }

    public List<MovieReservation> getAllReservations(){
        return movieReservationRepository.findAll();
    }

    public void confirmReservation(int reservationId){
        MovieReservation movieReservation = movieReservationRepository.findOne(reservationId);
        movieReservation.setConfirmed(true);
        movieReservationRepository.save(movieReservation);
    }

    public MovieReservation getMovieReservationById(Integer id){
        return movieReservationRepository.findOne(id);
    }
}
