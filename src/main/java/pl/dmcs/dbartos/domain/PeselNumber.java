package pl.dmcs.dbartos.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@ToString
@Getter
@Setter
@Entity
public class PeselNumber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private long peselNumber;

    @OneToOne(mappedBy="pesel")
    private User user;
}
