package pl.dmcs.dbartos.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@ToString
@Getter
@Setter
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private String description;
    private BigDecimal price;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    @ToString.Exclude
    @Lob
    @Column(length = 1000)
    private String thumbnail;
}
