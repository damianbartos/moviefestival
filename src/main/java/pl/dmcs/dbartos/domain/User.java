package pl.dmcs.dbartos.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;
import pl.dmcs.dbartos.utils.validator.Login;
import pl.dmcs.dbartos.utils.validator.Telephone;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ToString
@Getter
@Setter
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Size(min = 2, message = "validation.firstName")
    @NotNull
    private String firstName;

    @Size(min = 2, message = "validation.lastName")
    @NotNull
    private String lastName;

    @Email(message = "validation.email")
    @NotNull
    private String email;

    @Telephone(message = "validation.telephone")
    @NotNull
    private String telephone;

    @Size(min = 4, message = "validation.login")
    @Login(message = "validation.login.used")
    @NotNull
    private String login;

    @Size(min = 4, message = "validation.password")
    @NotNull
    private String password;

    private boolean enabled = false;

    @OrderBy(value = "id")
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<UserRole> userRole = new HashSet<>(0);

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Address address;

    @OneToOne(cascade = CascadeType.PERSIST)
    private PeselNumber pesel;

    @OrderBy(value = "id")
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id")
    private List<MovieReservation> reservedMovies = new ArrayList<>(0);

}

