//package pl.dmcs.dbartos.utils.validator;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.validation.Errors;
//import org.springframework.validation.Validator;
//import pl.dmcs.dbartos.domain.User;
//import pl.dmcs.dbartos.service.UserService;
//
//@Slf4j
//public class UserValidator implements Validator {
//
//
//    @Autowired
//    private UserService userService;
//
//    @Override
//    public boolean supports(Class<?> aClass) {
//        return User.class.equals(aClass);
//    }
//
//    @Override
//    public void validate(Object o, Errors errors) {
//        User userToValidate = (User) o;
//        log.info("validate of user - {}", userToValidate);
//        User user = userService.findByLogin(userToValidate.getLogin());
//        if(user != null){
//            log.error("user login is already used");
//            errors.rejectValue("login", "used", new Object[]{"login"}, "is already used");
//        }
//        if(!PhoneValidator.isValid(userToValidate.getTelephone())){
//            log.error("phone is not valid");
//            errors.rejectValue("telephone", "format", new Object[]{"telephone"}, "should contains 6 digits");
//        }
//    }
//}
