package pl.dmcs.dbartos.utils.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Telephone, String> {


    @Override
    public void initialize(Telephone telephone) {

    }

    @Override
    public boolean isValid(String phoneNumber, ConstraintValidatorContext constraintValidatorContext) {
        if(phoneNumber == null){
            return false;
        }
        //validate phone numbers of format "123456789"
        if (phoneNumber.matches("\\d{9}")) return true;
            //validating phone number with -, . or spaces
        else if(phoneNumber.matches("\\d{3}[-.\\s]\\d{3}[-.\\s]\\d{3}")) return true;
            //return false if nothing matches the input
        else return false;
    }
}
