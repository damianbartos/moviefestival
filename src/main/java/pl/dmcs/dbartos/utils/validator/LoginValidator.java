package pl.dmcs.dbartos.utils.validator;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.service.UserService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Setter
@Getter
@Slf4j
public class LoginValidator implements ConstraintValidator<Login, String> {
    @Autowired
    private UserService userService;

    @Override
    public void initialize(Login login) {

    }

    @Override
    public boolean isValid(String login, ConstraintValidatorContext constraintValidatorContext) {
        log.info("validate of user with login - {}", login);
        User user = userService.findByLogin(login);
        return user == null;
    }
}
