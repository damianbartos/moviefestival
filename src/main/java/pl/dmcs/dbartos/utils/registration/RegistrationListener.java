package pl.dmcs.dbartos.utils.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import pl.dmcs.dbartos.domain.User;
import pl.dmcs.dbartos.service.UserService;
import pl.dmcs.dbartos.service.VerificationTokenService;

import java.util.UUID;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private final MessageSource messageSource;
    private final UserService userService;
    private final VerificationTokenService verificationTokenService;
    private final JavaMailSender mailSender;

    @Autowired
    public RegistrationListener(MessageSource messageSource, UserService userService, VerificationTokenService verificationTokenService, JavaMailSender mailSender) {
        this.messageSource = messageSource;
        this.userService = userService;
        this.verificationTokenService = verificationTokenService;
        this.mailSender = mailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent onRegistrationCompleteEvent) {
        this.confirmRegistration(onRegistrationCompleteEvent);
    }
    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        verificationTokenService.createVerificationToken(user, token);

        String recipientAddress = user.getEmail();
        String subject = "Confirm Registration";
        String confirmationUrl = "http://localhost:8080" + event.getAppUrl() + "/registrationConfirm.html?token=" + token;

        StringBuilder messageContentBuilder = new StringBuilder();
        messageContentBuilder.append("Link confirmation");
        messageContentBuilder.append(System.lineSeparator());
        messageContentBuilder.append(confirmationUrl);

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setFrom("MovieFestival@festival.com");
        email.setSubject(subject);
        email.setText(messageContentBuilder.toString());
        mailSender.send(email);
    }
}
