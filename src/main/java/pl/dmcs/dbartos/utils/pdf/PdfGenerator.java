package pl.dmcs.dbartos.utils.pdf;

import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.springframework.core.io.ClassPathResource;
import pl.dmcs.dbartos.domain.Movie;
import pl.dmcs.dbartos.domain.MovieReservation;
import pl.dmcs.dbartos.domain.User;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

@Slf4j
public class PdfGenerator {

    static public ByteArrayInputStream generateTicketPdf(User user, MovieReservation movieReservation) {
        Movie movie = movieReservation.getMovie();
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage(page);
        ByteArrayOutputStream out = null;
        try {
            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            File file = new ClassPathResource("/font/arial/arial.ttf").getFile();
            PDFont arialFont = PDType0Font.load(document, file);
            contentStream.setFont(arialFont, 22);
            contentStream.beginText();
            contentStream.newLineAtOffset(25, 700);
            contentStream.showText("Movie ticket");
            contentStream.endText();

            contentStream.setFont(arialFont, 15);
            contentStream.beginText();
            contentStream.newLineAtOffset(25, 650);
            contentStream.setLeading(15f);
            contentStream.showText("Client info");
            contentStream.newLine();
            contentStream.showText(user.getFirstName() + " " + user.getLastName());
            contentStream.newLine();
            if(user.getAddress()!=null){
                contentStream.showText(user.getAddress().getStreet() + " " + user.getAddress().getNumber());
                contentStream.newLine();
                contentStream.showText(user.getAddress().getCity() + ", " + user.getAddress().getPostalCode());
                contentStream.newLine();
            }else {
                contentStream.showText("----------");
                contentStream.newLine();
                contentStream.showText("----------");
                contentStream.newLine();
            }
            contentStream.endText();

            contentStream.setFont(arialFont, 20);
            contentStream.beginText();
            contentStream.newLineAtOffset(25, 550);
            contentStream.setLeading(22f);
            contentStream.showText("movie info");
            contentStream.newLine();
            contentStream.showText(movie.getName());
            contentStream.newLine();
            contentStream.showText(movie.getDescription());
            contentStream.newLine();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
            contentStream.showText(simpleDateFormat.format(movie.getDate()));
            contentStream.newLine();
            contentStream.showText(movie.getPrice().toString() + " PLN");

            contentStream.endText();
            contentStream.close();
            out = new ByteArrayOutputStream();
            document.save(out);
            document.close();
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            log.error("pdf generation error");
            e.printStackTrace();
        }
        return null;
    }

}
