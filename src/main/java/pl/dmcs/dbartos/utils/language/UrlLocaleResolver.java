package pl.dmcs.dbartos.utils.language;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@Slf4j
public class UrlLocaleResolver implements LocaleResolver {

    private static final String URL_LOCALE_ATTRIBUTE_NAME = "URL_LOCALE_ATTRIBUTE_NAME";

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String uri = request.getRequestURI();

        log.info("language request URI={}", uri);

        String prefixEn = request.getServletContext().getContextPath() + "/en/";
        String prefixPl = request.getServletContext().getContextPath() + "/pl/";
        String prefixDe = request.getServletContext().getContextPath() + "/de/";

        Locale locale = null;

        if (uri.startsWith(prefixEn)) {
            locale = Locale.ENGLISH;
        } else if (uri.startsWith(prefixPl)) {
            locale = new Locale("pl", "PL");
        } else if (uri.startsWith(prefixDe)) {
            locale = Locale.GERMAN;
        }

        if (locale != null) {
            request.getSession().setAttribute(URL_LOCALE_ATTRIBUTE_NAME, locale);
        }

        if (locale == null) {
            locale = (Locale) request.getSession().getAttribute(URL_LOCALE_ATTRIBUTE_NAME);
            if (locale == null) {
                locale = Locale.ENGLISH;
            }
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        // Nothing
    }

}
